import { BrowserModule }        from '@angular/platform-browser';
import { NgModule }             from '@angular/core';
import { AppComponent }         from './app.component';
import { HeroJobAdComponent }   from './BannerTypes/hero-job-ad.component';
import { AdBannerComponent }    from './adcomponent/ad-banner.component';
import { HeroProfileComponent } from './BannerTypes/hero-profile.component';
import { AdDirective }          from './adcomponent/ad.directive';
import { AdService }            from './adcomponent/ad.service';

@NgModule({
  imports: [ BrowserModule ],
  providers: [AdService],
  declarations: [ AppComponent,
                  AdBannerComponent,
                  HeroJobAdComponent,
                  HeroProfileComponent,
                  AdDirective ],
  entryComponents: [ HeroJobAdComponent, HeroProfileComponent ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  constructor() {}
}

