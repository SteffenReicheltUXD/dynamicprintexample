import { Component, Input, OnInit, ViewChild, ComponentFactoryResolver, OnDestroy, AfterViewChecked } from '@angular/core';

import { AdDirective } from './ad.directive';
import { AdItem } from './ad-item';
import { AdComponent } from './ad.component';

@Component({
  selector: 'app-ad-banner',
  template: `
              <div class="ad-banner-example">
                <h3>Advertisements</h3>
                <hero-job [data]="data"></hero-job>
                <ng-template ad-host></ng-template>
              </div>
              <button (click)="printAll()">Print All Advertisments</button>
            `
})
export class AdBannerComponent implements AfterViewChecked {
  @Input() ads: AdItem[];
  currentAdIndex = -1;
  @ViewChild(AdDirective) adHost: AdDirective;

  data = {
    headline: 'We want you!',
    body: 'Schick uns deine APAX Daten'
  };

  private print = false;
  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  printAll() {
    let viewContainerRef = this.adHost.viewContainerRef;
    viewContainerRef.clear();

    for (let index = 0; index < this.ads.length; index++) {
      const adItem = this.ads[index];
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(adItem.component);
      const componentRef = viewContainerRef.createComponent(componentFactory);
      (<AdComponent>componentRef.instance).data = adItem.data;
    }
    this.print = true;
  }

  ngAfterViewChecked() {
    console.log('checked');
    if (this.print == true) {
      window.print();
      let viewContainerRef = this.adHost.viewContainerRef;
      viewContainerRef.clear();
      this.print = false;
    }
  }
}